# #%L
# OME Files View libraries (cmake build infrastructure)
# %%
# Copyright © 2006 - 2015 Open Microscopy Environment:
#   - Massachusetts Institute of Technology
#   - National Institutes of Health
#   - University of Dundee
#   - Board of Regents of the University of Wisconsin-Madison
#   - Glencoe Software, Inc.
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config-internal.h.in
               ${CMAKE_CURRENT_BINARY_DIR}/config-internal.h @ONLY)

set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(view_SOURCES
    view.cpp
    Window.cpp
    GLView2D.cpp
    module.cpp
    NavigationDock2D.cpp
    TexelProperties.cpp
    gl/Axis2D.cpp
    gl/Grid2D.cpp
    gl/Image2D.cpp
    gl/Util.cpp
    glsl/GLFlatShader2D.cpp
    glsl/GLImageShader2D.cpp
    glsl/GLLineShader2D.cpp)

set(view_HEADERS
    Window.h
    GLView2D.h
    glm.h
    module.h
    NavigationDock2D.h
    TexelProperties.h
    gl/Axis2D.h
    gl/Grid2D.h
    gl/Image2D.h
    gl/Util.h
    glsl/GLFlatShader2D.h
    glsl/GLImageShader2D.h
    glsl/GLLineShader2D.h
    ${CMAKE_CURRENT_BINARY_DIR}/config-internal.h)

add_executable(view
               ${view_SOURCES}
               ${view_HEADERS}
               ${view_HEADERS_MOC}
               ${view_RESOURCES})


target_include_directories(view PUBLIC
                           $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>
                           $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src>
                           $<BUILD_INTERFACE:${OPENGL_INCLUDE_DIR}>
                           $<BUILD_INTERFACE:${GLM_INCLUDE_DIR}>)

target_link_libraries(view OME::Files)
if (Qt6_FOUND)
    target_link_libraries(view Qt6::Core Qt6::Gui Qt6::OpenGL Qt6::OpenGLWidgets Qt6::Widgets Qt6::Svg)
else()
    target_link_libraries(view Qt5::Core Qt5::Gui Qt5::Widgets Qt5::Svg)
endif()
target_link_libraries(view ${OPENGL_gl_LIBRARY})

install(TARGETS view RUNTIME
        DESTINATION ${OME_FILES_VIEW_INSTALL_PKGLIBEXECDIR}
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
                    GROUP_READ GROUP_EXECUTE
                    WORLD_READ WORLD_EXECUTE
        COMPONENT "runtime")
